package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`, "\n")

	got := part01(input, 5)
	expected := 127
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}

}

func TestPart02(t *testing.T) {

	input := strings.Split(`35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`, "\n")

	got := part02(input, 5)
	expected := 62
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}
}
