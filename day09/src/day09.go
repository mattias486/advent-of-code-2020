package main

import (
	"fmt"
	"strconv"
)

func part01(rows []string, preamble int) int {

	queue := getQueue(rows)

	return findFirstInvalid(queue, preamble)
}

func part02(rows []string, preamble int) int {

	queue := getQueue(rows)

	sumToFind := findFirstInvalid(queue, preamble)

	return findNumbersWithTotalSum(queue, preamble, sumToFind)
}

func getQueue(rows []string) []int {

	queue := []int{}
	for _, rowItem := range rows {
		number, _ := strconv.Atoi(rowItem)
		queue = append(queue, number)
	}
	return queue
}

func findPair(numbers []int, sum int) (int, int, bool) {
	for i, first := range numbers {
		for j, second := range numbers {
			if j <= i {
				continue
			}
			if first+second == sum {
				return first, second, true
			}
		}
	}
	return -1, -1, false

}

func hasPair(value int, numbers []int) bool {
	for i, first := range numbers {
		for j, second := range numbers {
			if j <= i {
				continue
			}
			if first+second == value {
				return true
			}
		}
	}
	return false
}

func findFirstInvalid(numbers []int, preamble int) int {

	for index, value := range numbers {
		if index < preamble {
			continue
		}
		if !hasPair(numbers[index], numbers[index-preamble:index]) {
			return value
		}
	}
	return -1
}

func getSumOf(numbers []int) int {
	result := 0
	for _, value := range numbers {
		result += value
	}
	return result
}

func getMaxOf(numbers []int) int {
	if len(numbers) == 0 {
		return 0
	}
	result := numbers[0]
	for _, value := range numbers {
		if value > result {
			result = value
		}
	}
	return result
}

func getMinOf(numbers []int) int {
	if len(numbers) == 0 {
		return 0
	}
	result := numbers[0]
	for _, value := range numbers {
		if value < result {
			result = value
		}
	}
	return result
}

func findNumbersWithTotalSum(numbers []int, preamble int, sumToFind int) int {

	for index, _ := range numbers {
		localNumbers := []int{}
		sum := 0
		i := index
		for sum < sumToFind && i < len(numbers) {
			localNumbers = append(localNumbers, numbers[i])
			sum = getSumOf(localNumbers)
			if len(localNumbers) > 1 && sum == sumToFind {
				minAndMax := getMaxOf(localNumbers) + getMinOf(localNumbers)
				fmt.Println("Found sum", minAndMax, localNumbers)
				return minAndMax
			}
			i++
		}

	}
	return -1
}

func main() {

	rows := rowsAsStringArray("input")

	part01 := part01(rows, 25)

	fmt.Println("Part01", part01)

	part02 := part02(rows, 25)

	fmt.Println("Part02", part02)
}
