package main

import (
	"fmt"
)

func getTreePositions(row string) map[int]bool {
	positions := make(map[int]bool)
	const tree = '#'
	for index, char := range row {
		if char == tree {
			positions[index] = true
		}
	}
	return positions
}

func getRowsOfPositions(rows []string) []map[int]bool {
	trees := make([]map[int]bool, len(rows))
	for index, row := range rows {
		trees[index] = getTreePositions(row)
	}
	return trees
}

type Direction struct {
	deltaX int
	deltaY int
}

type Position struct {
	x int
	y int
}

func hasTreeOnPosition(treeRow map[int]bool, rowLength int, x int) bool {

	normalizedX := x % rowLength

	if _, isTree := treeRow[normalizedX]; isTree {
		return true
	}
	return false
}

func getNumberOfTrees(treePositions []map[int]bool, treeRowLength int, direction Direction) int {
	result := 0
	position := Position{0, 0}
	for {
		if position.y >= len(treePositions) {
			fmt.Println(result)
			return result
		}
		if hasTreeOnPosition(treePositions[position.y], treeRowLength, position.x) {
			result++
		}

		position.x += direction.deltaX
		position.y += direction.deltaY
	}
}

func main() {

	rows := rowsAsStringArray("input")
	treePositions := getRowsOfPositions(rows)

	treeRowLength := len(rows[0])

	directionPart01 := Direction{3, 1}

	numberOfTrees := getNumberOfTrees(treePositions, treeRowLength, directionPart01)
	fmt.Println("Number of trees (Part01)", numberOfTrees)

	directionsPart02 := []Direction{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}

	multipliedDirections := 1
	for _, direction := range directionsPart02 {
		multipliedDirections *= getNumberOfTrees(treePositions, treeRowLength, direction)
	}
	fmt.Println("Number of trees (Part02)", multipliedDirections)
}
