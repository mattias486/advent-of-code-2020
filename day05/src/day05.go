package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func parseSeatId(rowAndSeat string) int {
	rowAndSeatBinary := strings.ReplaceAll(rowAndSeat, "F", "0")
	rowAndSeatBinary = strings.ReplaceAll(rowAndSeatBinary, "B", "1")
	rowAndSeatBinary = strings.ReplaceAll(rowAndSeatBinary, "L", "0")
	rowAndSeatBinary = strings.ReplaceAll(rowAndSeatBinary, "R", "1")
	row, _ := strconv.ParseInt(rowAndSeatBinary[0:7], 2, 16)
	seat, _ := strconv.ParseInt(rowAndSeatBinary[7:10], 2, 16)
	return int(row*8 + seat)
}

func part01(rows []string) int {
	maxRowAndSeat := 0
	for _, rowItem := range rows {
		seatId := parseSeatId(rowItem)
		if seatId > maxRowAndSeat {
			maxRowAndSeat = seatId
		}
	}
	return maxRowAndSeat
}

func findEmptySeatEmpty(rows []string) int {
	seatIds := make([]int, len(rows))
	for _, rowItem := range rows {
		seatIds = append(seatIds, parseSeatId(rowItem))
	}
	sort.Ints(seatIds)
	for i, seatId := range seatIds {
		if i == len(seatIds)-2 {
			break
		}
		isEmptySeat := seatIds[i+1] == seatId+2
		if isEmptySeat {
			return seatId + 1
		}
	}
	return 0
}

func main() {

	rows := rowsAsStringArray("input")
	part01 := part01(rows)

	part02 := findEmptySeatEmpty(rows)

	fmt.Println("Highest seat id : ", part01)
	fmt.Println("Empty seat id : ", part02)
}
