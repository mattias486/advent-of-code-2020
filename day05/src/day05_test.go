package main

import (
	"testing"
)

func TestParseSeatId(t *testing.T) {

	input := []string{"BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"}
	expected := [3]int{567, 119, 820}
	results := [3]int{}
	for i, inputItem := range input {
		results[i] = parseSeatId(inputItem)
	}
	if results != expected {
		t.Errorf("ParseSeatId = %v; want %v", results, expected)
	}
}

func TestPart01(t *testing.T) {
	/*passports := strings.Split(`some testdata
	from day05
	`, "\n")

		expected := 2
		got := part01(passports)
		if got != expected {
			t.Errorf("Part01 = %d; want %d", got, expected)
		}*/
}
