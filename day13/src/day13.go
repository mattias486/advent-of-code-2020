package main

import (
	"fmt"
)

func part01(earliestTime int, busses []int) int {

	lowestTimeDiff := 0
	lowestBussId := 0
	for _, bussItem := range busses {

		minutesSinceLast := earliestTime % bussItem
		nextTimeDiff := bussItem - minutesSinceLast

		if lowestTimeDiff == 0 || nextTimeDiff < lowestTimeDiff {
			lowestTimeDiff = nextTimeDiff
			lowestBussId = bussItem
		}
	}
	return lowestTimeDiff * lowestBussId
}

type BussItem struct {
	id                int64
	expectedRemainder int64
}

func compensateNegativeRemainder(bussId int, remainder int64) int64 {
	result := remainder
	for {
		if result >= 0 {
			return result
		}
		result += int64(bussId)
	}
}

func createBussItems(busses []int) []BussItem {
	result := []BussItem{}
	for i, bussId := range busses {
		if bussId == 0 {
			continue
		}
		expectedRemainder := int64((bussId*3 - i) % bussId)
		expectedRemainder = compensateNegativeRemainder(bussId, expectedRemainder)
		result = append(result, BussItem{int64(bussId), expectedRemainder})
	}
	return result
}

func getHighestBussId(bussItems []BussItem) BussItem {

	result := BussItem{}
	for _, bussItem := range bussItems {
		if (result == BussItem{} || bussItem.id > result.id) {
			result = bussItem
		}
	}
	return result
}

func doesNumberMatchAllItems(number int64, bussItems []BussItem) bool {
	for _, item := range bussItems {
		if !doesNumberMatchBussItem(number, item) {
			return false
		}
	}
	return true
}

func doesNumberMatchBussItem(number int64, item BussItem) bool {
	return number%item.id == item.expectedRemainder
}

func doesNumberMatchBoth(number int64, first BussItem, second BussItem) bool {
	return doesNumberMatchBussItem(number, first) && doesNumberMatchBussItem(number, second)
}

func findNumberMatching(frequency int64, loopItem BussItem, bussItems []BussItem) int64 {
	i := frequency
	first := bussItems[0]
	second := bussItems[1]
	for {
		if doesNumberMatchBoth(i, first, second) {
			return i
		}
		i += loopItem.id
	}
	return 0
}

func findLoopIntervalBetweenPairs(offset int64, frequency int64, first BussItem, second BussItem) (int64, int64) {

	firstMatch := int64(0)
	secondMatch := int64(0)

	i := offset
	for {
		if !doesNumberMatchBoth(i, first, second) {
			i += frequency
			continue
		}
		if firstMatch != 0 {
			secondMatch = i
			return firstMatch, secondMatch - firstMatch
		}
		firstMatch = i
		i += frequency
	}
}

func findNumberMatchingV2(bussItems []BussItem) int64 {

	frequency := int64(1)
	offset := int64(0)
	for key, _ := range bussItems {
		if key == 0 {
			continue
		}
		last := bussItems[key-1]
		current := bussItems[key]
		offset, frequency = findLoopIntervalBetweenPairs(offset, frequency, last, current)
	}
	return offset
}

func part02(busses []int) int64 {

	bussItems := createBussItems(busses)

	result := findNumberMatchingV2(bussItems)

	return result
}

func main() {

	earliestTime := 1006697
	busses := []int{13, 41, 641, 19, 17, 29, 661, 37, 23}

	part01 := part01(earliestTime, busses)
	fmt.Println("Part01", part01)

	busses = []int{13, 0, 0, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 641, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 661, 0, 0, 0, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23}

	part02 := part02(busses)
	fmt.Println("Part02", part02)

}
