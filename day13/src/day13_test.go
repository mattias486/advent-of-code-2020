package main

import (
	"testing"
)

func TestPart01(t *testing.T) {

	earliest := 939
	activeBussIds := []int{7, 13, 59, 31, 19}

	got := part01(earliest, activeBussIds)
	expected := 295
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}
}

func TestPart02(t *testing.T) {

	activeBussIds := []int{17, 0, 13, 19}
	got := part02(activeBussIds)
	expected := int64(3417)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

	activeBussIds = []int{67, 7, 59, 61}
	got = part02(activeBussIds)
	expected = int64(754018)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

	activeBussIds = []int{67, 0, 7, 59, 61}
	got = part02(activeBussIds)
	expected = int64(779210)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

	activeBussIds = []int{67, 7, 0, 59, 61}
	got = part02(activeBussIds)
	expected = int64(1261476)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

	activeBussIds = []int{1789, 37, 47, 1889}
	got = part02(activeBussIds)
	expected = int64(1202161486)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

	activeBussIds = []int{7, 13, 0, 0, 59, 0, 31, 19}

	got = part02(activeBussIds)
	expected = int64(1068781)
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}

}
