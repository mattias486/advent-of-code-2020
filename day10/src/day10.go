package main

import (
	"fmt"
	"sort"
)

func getDiffs(sorted []int) []int {
	result := []int{}
	for key, val := range sorted {
		if key == 0 {
			continue
		}
		result = append(result, val-sorted[key-1])
	}
	return result
}

func getNumbersMatching(diffs []int, wantedDiff int) int {
	result := 0
	for _, val := range diffs {
		if val == wantedDiff {
			result += 1
		}
	}
	return result
}

func part01(input []int) int {

	diffs := getSortedDiffs(input)

	diffOfOne := getNumbersMatching(diffs, 1)
	diffOfThree := getNumbersMatching(diffs, 3)

	return diffOfOne * diffOfThree
}

func getMaxValue(input []int) int {
	max := 0
	for _, val := range input {
		if val > max {
			max = val
		}
	}
	return max
}

func getSortedDiffs(input []int) []int {

	modifiedInput := make([]int, len(input))
	copy(modifiedInput, input)

	lastElement := getMaxValue(modifiedInput) + 3

	modifiedInput = append(modifiedInput, 0)
	modifiedInput = append(modifiedInput, lastElement)

	sort.Ints(modifiedInput)

	diffs := getDiffs(modifiedInput)
	return diffs
}

func getMultiplicer(sequence int) int {
	switch sequence {
	case 4:
		return 7
	case 3:
		return 4
	case 2:
		return 2
	default:
		return 1
	}
}

func part02(input []int) int {

	diffs := getSortedDiffs(input)

	combinations := 1
	valueOneCounter := 0
	for _, val := range diffs {
		if val == 3 {
			combinations *= getMultiplicer(valueOneCounter)
			valueOneCounter = 0
		} else if val == 1 {
			valueOneCounter += 1
		}
	}
	return combinations
}

func main() {

	input := rowsAsIntArray("input")

	part01 := part01(input)

	fmt.Println("Part01", part01)

	part02 := part02(input)

	fmt.Println("Part02", part02)
}
