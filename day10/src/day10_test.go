package main

import (
	"testing"
)

func TestPart01(t *testing.T) {

	input := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}

	got := part01(input)
	expected := 7 * 5
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}
}

func TestPart01Larger(t *testing.T) {

	input := []int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}

	got := part01(input)
	expected := 22 * 10
	if got != expected {
		t.Errorf("Part01Larger = %d, want : %d", got, expected)
	}
}

func TestPart02(t *testing.T) {

	input := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}

	got := part02(input)
	expected := 8
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}
}

func TestPart02Larger(t *testing.T) {

	input := []int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}

	got := part02(input)
	expected := 19208
	if got != expected {
		t.Errorf("Part02Larger = %d, want : %d", got, expected)
	}
}
