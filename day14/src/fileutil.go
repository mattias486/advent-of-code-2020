package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func rowsAsStringArray(path string) []string {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	strings := strings.Split(string(bytes), "\n")
	// skip last empty item after last new line
	return strings[0 : len(strings)-1]
}

func rowsAsIntArray(path string) []int {
	rows := rowsAsStringArray(path)
	result := []int{}
	for _, row := range rows {
		val, _ := strconv.Atoi(row)
		result = append(result, val)
	}
	return result
}
