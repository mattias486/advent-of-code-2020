package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0`, "\n")

	expected := uint64(165)
	got := part01(input)
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}
}

func TestPart02(t *testing.T) {
	input := strings.Split(`mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1`, "\n")

	expected := uint64(208)
	got := part02(input)
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}

}
