package main

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func reverseString(value string) string {
	result := ""
	for _, char := range value {
		result = string(char) + result
	}
	return result
}

func maskBitZero(value uint64, index int) uint64 {
	complement := getComplementForZero(index)
	return complement & value
}

func maskBitOne(value uint64, index int) uint64 {
	result := uint64(math.Pow(2, float64(index))) | value
	return result
}

func getComplementForZero(index int) uint64 {
	complement := (1<<64 - 1) ^ uint64(math.Pow(2, float64(index)))
	return complement
}

func applyMask(mask string, value uint64) uint64 {
	reversedMask := reverseString(mask)
	for i, val := range reversedMask {
		valString := string(val)
		if valString == "X" {
			continue
		}
		if valString == "1" {
			value = maskBitOne(value, i)

		} else if valString == "0" {
			value = maskBitZero(value, i)
		}
	}
	return value
}

func getSumOfMemory(mem map[int]uint64) uint64 {
	sum := uint64(0)
	for _, v := range mem {
		sum += v
	}
	return sum
}

func getSumOfMemoryU64(mem map[uint64]uint64) uint64 {
	sum := uint64(0)
	for _, v := range mem {
		sum += v
	}
	return sum
}

func part01(input []string) uint64 {

	var mask string

	memInstructionMatcher := regexp.MustCompile("^mem\\[([0-9]+)\\] = ([0-9]+)$")
	mem := map[int]uint64{}

	for _, row := range input {
		instruction := strings.Split(row, " = ")
		if instruction[0] == "mask" {
			mask = instruction[1]
		} else {
			matches := memInstructionMatcher.FindStringSubmatch(row)

			memId, _ := strconv.Atoi(matches[1])
			memValue, _ := strconv.ParseInt(matches[2], 10, 64)
			memValueUnsigned := uint64(memValue)

			memValueAfterMask := applyMask(mask, memValueUnsigned)
			mem[memId] = memValueAfterMask
		}
	}

	return getSumOfMemory(mem)
}

func getFloatingPair(address uint64, floatingIndex int) []uint64 {
	result := []uint64{
		maskBitOne(address, floatingIndex),
		maskBitZero(address, floatingIndex),
	}
	return result
}

func getMemoryAddressFromFloating(address uint64, floatingIndexes []int) []uint64 {
	if len(floatingIndexes) == 0 {
		return []uint64{address}
	}

	pair := getFloatingPair(address, floatingIndexes[0])
	result := []uint64{}
	for _, val := range pair {
		for _, val2 := range getMemoryAddressFromFloating(val, floatingIndexes[1:]) {
			result = append(result, val2)
		}
	}
	return result
}

func applyAddressMask(mask string, address uint64) []uint64 {
	reversedMask := reverseString(mask)
	floatingIndexes := []int{}
	modifiedAddress := address
	for i, val := range reversedMask {
		valString := string(val)
		if valString == "X" {
			floatingIndexes = append(floatingIndexes, i)
			continue
		}
		if valString == "1" {
			modifiedAddress = maskBitOne(modifiedAddress, i)
		}
	}
	return getMemoryAddressFromFloating(modifiedAddress, floatingIndexes)
}

func part02(input []string) uint64 {

	var mask string

	memInstructionMatcher := regexp.MustCompile("^mem\\[([0-9]+)\\] = ([0-9]+)$")
	mem := map[uint64]uint64{}

	for _, row := range input {
		instruction := strings.Split(row, " = ")
		if instruction[0] == "mask" {
			mask = instruction[1]
		} else {
			matches := memInstructionMatcher.FindStringSubmatch(row)

			memId, _ := strconv.ParseInt(matches[1], 10, 64)
			memIdUnsigned := uint64(memId)

			memValue, _ := strconv.ParseInt(matches[2], 10, 64)
			memValueUnsigned := uint64(memValue)

			memIdsAfterMask := applyAddressMask(mask, memIdUnsigned)
			for _, memIdItem := range memIdsAfterMask {
				mem[memIdItem] = memValueUnsigned
			}
		}
	}

	return getSumOfMemoryU64(mem)
}

func main() {

	input := rowsAsStringArray("input")

	part01 := part01(input)

	fmt.Println("Part01", part01)

	part02 := part02(input)

	fmt.Println("Part02", part02)
}
