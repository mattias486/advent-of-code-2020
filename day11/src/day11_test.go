package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`, "\n")

	got := part01(input)
	expected := 37
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}
}

func TestPart02(t *testing.T) {

	input := strings.Split(`L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`, "\n")

	got := part02(input)
	expected := 26
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}
}
