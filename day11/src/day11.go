package main

import (
	"fmt"
	"reflect"
)

type Position struct {
	isFloor    bool
	isOccupied bool
}

func parsePositions(input []string) [][]Position {
	positions := make([][]Position, len(input))
	for y, inputRow := range input {
		positions[y] = make([]Position, len(inputRow))
		for x, rowItem := range inputRow {
			switch rowItem {
			case '.':
				positions[y][x] = Position{true, false}
			case 'L':
				positions[y][x] = Position{false, false}
			}
		}
	}
	return positions
}

type OccupiedValidator func(x int, y int, positions [][]Position) int

func getNearbyOccupied() OccupiedValidator {
	return func(x int, y int, positions [][]Position) int {
		result := 0
		for dY := -1; dY <= 1; dY++ {
			for dX := -1; dX <= 1; dX++ {
				if dY == 0 && dX == 0 {
					continue
				}
				iX := x + dX
				iY := y + dY
				if iY >= 0 && iY < len(positions) {
					if iX >= 0 && iX < len(positions[0]) {
						if positions[iY][iX].isOccupied {
							result++
						}
					}
				}
			}
		}
		return result
	}
}

func isFirstVisibleOccupiedInDirection(x int, y int, dX int, dY int, positions [][]Position) bool {

	iY := y + dY
	iX := x + dX
	for {
		if iY < 0 || iY >= len(positions) || iX < 0 || iX >= len(positions[0]) {
			return false
		}
		if positions[iY][iX].isFloor {
			iY += dY
			iX += dX
			continue
		}
		return positions[iY][iX].isOccupied
	}
	return false
}

func getSumOfBoolean(b ...bool) int {
	result := 0
	for _, value := range b {
		if value {
			result++
		}
	}
	return result
}

func getOccupiedBySight() OccupiedValidator {
	return func(x int, y int, positions [][]Position) int {
		return getSumOfBoolean(
			isFirstVisibleOccupiedInDirection(x, y, -1, -1, positions),
			isFirstVisibleOccupiedInDirection(x, y, 0, -1, positions),
			isFirstVisibleOccupiedInDirection(x, y, 1, -1, positions),

			isFirstVisibleOccupiedInDirection(x, y, -1, 0, positions),
			isFirstVisibleOccupiedInDirection(x, y, 1, 0, positions),

			isFirstVisibleOccupiedInDirection(x, y, -1, 1, positions),
			isFirstVisibleOccupiedInDirection(x, y, 0, 1, positions),
			isFirstVisibleOccupiedInDirection(x, y, 1, 1, positions),
		)
	}
}

func draw(positions [][]Position) {

	for y := 0; y < len(positions); y++ {
		row := ""
		for x := 0; x < len(positions[0]); x++ {
			char := " "
			positionItem := positions[y][x]
			if positionItem.isFloor {
				char = "."
			} else if positionItem.isOccupied {
				char = "#"
			} else {
				char = "L"
			}
			row += char
		}
		fmt.Println(row)
	}
}

func getCopyOfPositions(input [][]Position) [][]Position {
	positions := make([][]Position, len(input))
	for i := range positions {
		positions[i] = make([]Position, len(input[i]))
		copy(positions[i], input[i])
	}
	return positions
}

func tick(input [][]Position, maxNearbyToLeaveSeat int, validator OccupiedValidator) [][]Position {
	positions := getCopyOfPositions(input)

	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[0]); x++ {
			inputItem := input[y][x]
			targetItem := &positions[y][x]
			if inputItem.isFloor {
				continue
			}
			nearby := validator(x, y, input)
			if inputItem.isOccupied && nearby >= maxNearbyToLeaveSeat {
				targetItem.isOccupied = false
			} else if nearby == 0 {
				targetItem.isOccupied = true
			}
		}
	}
	return positions
}

func getOccupiedSeats(positions [][]Position) int {
	sum := 0
	for y := 0; y < len(positions); y++ {
		for x := 0; x < len(positions[0]); x++ {
			if positions[y][x].isOccupied {
				sum++
			}
		}
	}
	return sum
}

func part01(input []string) int {

	positions := parsePositions(input)
	//fmt.Println("Initial")
	//draw(positions)
	fmt.Println("==================")
	for i := 0; i < 100000; i++ {
		lastPositions := getCopyOfPositions(positions)

		positions = tick(positions, 4, getNearbyOccupied())
		//draw(positions)
		//fmt.Println("==================")
		if reflect.DeepEqual(positions, lastPositions) {
			return getOccupiedSeats(positions)
		}
	}
	return 0
}

func part02(input []string) int {
	positions := parsePositions(input)
	fmt.Println("Initial")
	//draw(positions)
	//fmt.Println("==================")
	for i := 0; i < 100000; i++ {
		lastPositions := getCopyOfPositions(positions)

		positions = tick(positions, 5, getOccupiedBySight())
		//draw(positions)
		//fmt.Println("==================")
		if reflect.DeepEqual(positions, lastPositions) {
			fmt.Println(i)
			return getOccupiedSeats(positions)
		}
	}
	return 0
}

func main() {
	input := rowsAsStringArray("input")

	part01 := part01(input)

	fmt.Println("Part 01", part01)

	part02 := part02(input)

	fmt.Println("Part 02", part02)
}
