package main

import (
	"fmt"
	"strconv"
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func getManhattanDistance(x0 int, y0 int, x1 int, y1 int) int {
	return abs(x0+x1) + abs(y0+y1)
}

const (
	EAST  = iota
	SOUTH = iota
	WEST  = iota
	NORTH = iota
)

type Direction int

type Direction2D struct {
	x int
	y int
}

func getLeftDirection(direction Direction, numberOfTurns int) Direction {
	left := (4 + int(direction) - numberOfTurns) % 4
	return Direction(left)
}

func getRightDirection(direction Direction, numberOfTurns int) Direction {
	right := (int(direction) + numberOfTurns) % 4
	return Direction(right)
}

func goNorth(x int, y int, distance int) (int, int) {
	return x, y - distance
}

func goSouth(x int, y int, distance int) (int, int) {
	return x, y + distance
}

func goEast(x int, y int, distance int) (int, int) {
	return x + distance, y
}

func goWest(x int, y int, distance int) (int, int) {
	return x - distance, y
}

func moveWayPointNorth(direction Direction2D, value int) Direction2D {
	direction.y += value
	return direction
}

func moveWayPointSouth(direction Direction2D, value int) Direction2D {
	direction.y -= value
	return direction
}

func moveWayPointEast(direction Direction2D, value int) Direction2D {
	direction.x += value
	return direction
}

func moveWayPointWest(direction Direction2D, value int) Direction2D {
	direction.x -= value
	return direction
}

func rotateDirectionRightOneTick(x int, y int) (int, int) {
	return y, 0 - x
}

func rotateDirectionLeftOneTick(x int, y int) (int, int) {
	return 0 - y, x
}

func rotateDirectionRight(direction Direction2D, numberOfTurns int) Direction2D {
	x, y := direction.x, direction.y
	for i := 0; i < numberOfTurns; i++ {
		x, y = rotateDirectionRightOneTick(x, y)
	}
	return Direction2D{x, y}
}

func rotateDirectionLeft(direction Direction2D, numberOfTurns int) Direction2D {
	x, y := direction.x, direction.y
	for i := 0; i < numberOfTurns; i++ {
		x, y = rotateDirectionLeftOneTick(x, y)
	}
	return Direction2D{x, y}
}

func moveForward(x int, y int, direction Direction2D) (int, int) {
	return x, y
}

type InstructionProcessor func(instruction string, direction Direction, x int, y int) (int, int, Direction)

func processInstruction(instruction string, direction Direction, x int, y int) (int, int, Direction) {
	instructionType := string(instruction[0])
	instructionValue, _ := strconv.Atoi(instruction[1:])

	switch instructionType {
	case "N":
		x, y := goNorth(x, y, instructionValue)
		return x, y, direction
	case "S":
		x, y := goSouth(x, y, instructionValue)
		return x, y, direction
	case "E":
		x, y := goEast(x, y, instructionValue)
		return x, y, direction
	case "W":
		x, y := goWest(x, y, instructionValue)
		return x, y, direction
	case "R":
		numberOfTurns := instructionValue / 90
		direction = getRightDirection(direction, numberOfTurns)
		return x, y, direction
	case "L":
		numberOfTurns := instructionValue / 90
		direction = getLeftDirection(direction, numberOfTurns)
		return x, y, direction
	case "F":
		switch direction {
		case EAST:
			x, y := goEast(x, y, instructionValue)
			return x, y, direction
		case WEST:
			x, y := goWest(x, y, instructionValue)
			return x, y, direction
		case NORTH:
			x, y := goNorth(x, y, instructionValue)
			return x, y, direction
		case SOUTH:
			x, y := goSouth(x, y, instructionValue)
			return x, y, direction
		}
	}
	return 0, 0, direction
}

func processInstructionWith2DDirection(instruction string, direction Direction2D, x int, y int) (int, int, Direction2D) {
	instructionType := string(instruction[0])
	instructionValue, _ := strconv.Atoi(instruction[1:])

	switch instructionType {
	case "N":
		direction = moveWayPointNorth(direction, instructionValue)
		return x, y, direction
	case "S":
		direction = moveWayPointSouth(direction, instructionValue)
		return x, y, direction
	case "E":
		direction = moveWayPointEast(direction, instructionValue)
		return x, y, direction
	case "W":
		direction = moveWayPointWest(direction, instructionValue)
		return x, y, direction
	case "R":
		numberOfTurns := instructionValue / 90
		direction = rotateDirectionRight(direction, numberOfTurns)
		return x, y, direction
	case "L":
		numberOfTurns := instructionValue / 90
		direction = rotateDirectionLeft(direction, numberOfTurns)
		return x, y, direction
	case "F":
		x += direction.x * instructionValue
		y += direction.y * instructionValue
		return x, y, direction
	}
	return 0, 0, direction
}

func part01(input []string) int {

	x0, y0, y, x := 0, 0, 0, 0
	direction := Direction(EAST)
	for _, row := range input {
		x, y, direction = processInstruction(row, direction, x, y)
	}
	return getManhattanDistance(x0, y0, x, y)

}

func part02(input []string) int {
	x0, y0, y, x := 0, 0, 0, 0
	direction2d := Direction2D{10, 1}
	for _, row := range input {
		x, y, direction2d = processInstructionWith2DDirection(row, direction2d, x, y)
	}
	return getManhattanDistance(x0, y0, x, y)
}

func main() {

	input := rowsAsStringArray("input")

	part01 := part01(input)

	fmt.Println("Part 01", part01)

	part02 := part02(input)

	fmt.Println("Part 02", part02)
}
