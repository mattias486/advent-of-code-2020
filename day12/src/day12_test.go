package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`F10
N3
F7
R90
F11`, "\n")

	got := part01(input)
	expected := 25
	if got != expected {
		t.Errorf("Part01 = %d, want : %d", got, expected)
	}
}

func TestPart02(t *testing.T) {

	input := strings.Split(`F10
N3
F7
R90
F11`, "\n")

	got := part02(input)
	expected := 286
	if got != expected {
		t.Errorf("Part02 = %d, want : %d", got, expected)
	}
}

func TestDirections(t *testing.T) {

	expected := Direction(SOUTH)
	_, _, got := processInstruction("L270", EAST, 0, 0)
	if got != expected {
		t.Errorf("TurnLeft = %d, want : %d", got, expected)
	}

	expected = Direction(WEST)
	_, _, got = processInstruction("L180", EAST, 0, 0)
	if got != expected {
		t.Errorf("TurnLeft = %d, want : %d", got, expected)
	}

	expected = Direction(NORTH)
	_, _, got = processInstruction("R270", EAST, 0, 0)
	if got != expected {
		t.Errorf("TurnRight = %d, want : %d", got, expected)
	}

	expected = Direction(WEST)
	_, _, got = processInstruction("R180", EAST, 0, 0)
	if got != expected {
		t.Errorf("TurnRight = %d, want : %d", got, expected)
	}

}

func TestRotations(t *testing.T) {

	given := Direction2D{10, 4}

	expected := Direction2D{4, -10}
	got := rotateDirectionRight(given, 1)
	if got != expected {
		t.Errorf("RotateRight = %d, want : %d", got, expected)
	}

	expected = Direction2D{-10, -4}
	got = rotateDirectionRight(given, 2)
	if got != expected {
		t.Errorf("RotateRight = %d, want : %d", got, expected)
	}

	expected = Direction2D{-4, 10}
	got = rotateDirectionRight(given, 3)
	if got != expected {
		t.Errorf("RotateRight = %d, want : %d", got, expected)
	}

	expected = Direction2D{-4, 10}
	got = rotateDirectionLeft(given, 1)
	if got != expected {
		t.Errorf("RotateLeft = %d, want : %d", got, expected)
	}

	expected = Direction2D{-10, -4}
	got = rotateDirectionLeft(given, 2)
	if got != expected {
		t.Errorf("RotateLeft = %d, want : %d", got, expected)
	}

	expected = Direction2D{4, -10}
	got = rotateDirectionLeft(given, 3)
	if got != expected {
		t.Errorf("RotateLeft = %d, want : %d", got, expected)
	}

}
