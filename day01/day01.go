package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func findMatchingPair() {

}

func main() {

	bytes, _ := ioutil.ReadFile("input")
	rows := strings.Split(string(bytes), "\n")

	fmt.Printf("Part 01\n")
	for firstIndex, first := range rows {
		var firstInt, _ = strconv.Atoi(first)
		for secondIndex, second := range rows {
			var secondInt, _ = strconv.Atoi(second)
			if firstIndex >= secondIndex {
				continue
			}
			var sum = firstInt + secondInt
			if sum == 2020 {
				var multiplied = firstInt * secondInt
				fmt.Printf("First : %d, Second : %d, Multiplied : %d\n", firstInt, secondInt, multiplied)
			}
		}
	}

	fmt.Printf("Part 02\n")
	for firstIndex, first := range rows {
		var firstInt, _ = strconv.Atoi(first)
		for secondIndex, second := range rows {
			var secondInt, _ = strconv.Atoi(second)
			if firstIndex >= secondIndex {
				continue
			}
			for thirdIndex, third := range rows {
				if secondIndex >= thirdIndex {
					continue
				}
				var thirdInt, _ = strconv.Atoi(third)
				var sum = firstInt + secondInt + thirdInt
				if sum == 2020 {
					var multiplied = firstInt * secondInt * thirdInt
					fmt.Printf("First : %d, Second : %d, Third : %d, Multiplied : %d\n", firstInt, secondInt, thirdInt, multiplied)
					return
				}
			}
		}
	}
}
