package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func part01(rows []string) int {
	bagType := "shiny gold"
	return canHoldBagOfType(bagType, rows)
}

func getContentMap(content []string) map[string]int {
	result := map[string]int{}
	matcher := regexp.MustCompile(`^([0-9]+) ([a-z]+ [a-z]+)( bags?)?$`)
	for _, contentItem := range content {
		if contentItem == "no other" {
			continue
		}
		matched := matcher.FindStringSubmatch(contentItem)
		metric, _ := strconv.Atoi(matched[1])
		bagType := matched[2]
		result[bagType] = metric
	}
	return result
}

func parseBags(rows []string) map[string]map[string]int {
	matcher := regexp.MustCompile(`^([a-z]+ [a-z]+) bags? contain ([a-z0-9, ]+) bags?\.`)

	results := map[string]map[string]int{}
	for _, rowItem := range rows {
		result := matcher.FindStringSubmatch(rowItem)
		key := result[1]
		contains := strings.Split(result[2], ", ")

		contentMap := getContentMap(contains)
		results[key] = contentMap
	}
	return results
}

func getBagsThatHoldColor(color string, bags map[string]map[string]int) []string {
	result := []string{}
	for key, bagContent := range bags {
		if _, ok := bagContent[color]; ok {
			result = append(result, key)
		}
	}
	return result
}

func canHoldBag(bagColor string, bags map[string]map[string]int) map[string]int {
	result := map[string]int{}

	canContainThisBag := getBagsThatHoldColor(bagColor, bags)
	for _, val := range canContainThisBag {
		result[val] = 1
	}

	for _, otherBagColor := range canContainThisBag {
		childHolds := canHoldBag(otherBagColor, bags)
		for k, _ := range childHolds {
			result[k] = 1
		}
	}
	return result
}

func canHoldBagOfType(bagType string, rows []string) int {
	bags := parseBags(rows)

	result := canHoldBag(bagType, bags)

	return len(result)
}

func getTotalBagMap(color string, bags map[string]map[string]int) map[string]int {

	result := map[string]int{}
	result[color] = 1
	for containedKey, containedValue := range bags[color] {
		subResult := getTotalBagMap(containedKey, bags)
		for subColor, numberOfBags := range subResult {
			result[subColor] += numberOfBags * containedValue
		}
	}
	fmt.Println(result)
	return result
}

func part02(rows []string) int {
	bagType := "shiny gold"

	bags := parseBags(rows)

	sum := 0
	for _, val := range getTotalBagMap(bagType, bags) {
		sum += val
	}
	sum -= 1 // remove the shiny gold bag itself from result
	return sum
}

func main() {

	rows := rowsAsStringArray("input")

	first := part01(rows)
	fmt.Println("Part 01", first)

	second := part02(rows)
	fmt.Println("Part 02", second)
}
