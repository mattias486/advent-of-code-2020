package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type PassportValidator func(password map[string]string) bool

func parsePasswords(rows []string) []map[string]string {
	passports := make([]map[string]string, 0)

	current := make(map[string]string)
	for _, row := range rows {
		if len(row) == 0 { // new passport
			passports = append(passports, current)
			current = make(map[string]string)
			continue
		}
		fields := strings.Split(row, " ")
		for _, fieldItem := range fields {
			keyValue := strings.Split(fieldItem, ":")
			current[keyValue[0]] = keyValue[1]
		}
	}
	passports = append(passports, current)
	return passports
}

func isValidPassportPart01() PassportValidator {
	return func(passport map[string]string) bool {
		wanted := map[string]int{
			"byr": 0,
			"iyr": 0,
			"eyr": 0,
			"hgt": 0,
			"hcl": 0,
			"ecl": 0,
			"pid": 0}

		for k, _ := range wanted {
			if _, ok := passport[k]; ok {
				continue
			}
			return false
		}
		return true
	}
}

func isInRange(length, min, max int, value string) bool {
	if len(value) != length {
		return false
	}
	valAsInt, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	if valAsInt < min || valAsInt > max {
		return false
	}
	return true
}

func isValidHeight(height string) bool {
	if !strings.HasSuffix(height, "cm") && !strings.HasSuffix(height, "in") {
		return false
	}
	unit := height[len(height)-2:]
	metric, err := strconv.Atoi(height[:len(height)-2])

	if err != nil {
		return false
	}
	switch unit {
	case "cm":
		return metric >= 150 && metric <= 193
	case "in":
		return metric >= 59 && metric <= 76
	}
	return false
}

func isValidEyeColor(val string) bool {
	validEcl := []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
	for _, color := range validEcl {
		if color == val {
			return true
		}
	}
	return false
}

func isValidPassportPart02() PassportValidator {
	return func(passport map[string]string) bool {
		wanted := map[string]int{
			"byr": 0,
			"iyr": 0,
			"eyr": 0,
			"hgt": 0,
			"hcl": 0,
			"ecl": 0,
			"pid": 0}

		for k, _ := range wanted {
			if _, ok := passport[k]; ok {
				continue
			}
			return false
		}

		for key, val := range passport {
			switch key {
			case "byr":
				if !isInRange(4, 1920, 2002, val) {
					return false
				}
			case "iyr":
				if !isInRange(4, 2010, 2020, val) {
					return false
				}

			case "eyr":
				if !isInRange(4, 2020, 2030, val) {
					return false
				}
			case "hgt":
				if !isValidHeight(val) {
					return false
				}
			case "hcl":
				if match, _ := regexp.MatchString("^#[a-f0-9]{6}$", val); !match {
					return false
				}
			case "ecl":
				if !isValidEyeColor(val) {
					return false
				}
			case "pid":
				if match, _ := regexp.MatchString("^[0-9]{9}$", val); !match {
					return false
				}
			}
		}
		return true
	}
}

func validatePasswords(rows []string, validator PassportValidator) int {

	validPassports := 0
	passports := parsePasswords(rows)
	for _, passport := range passports {
		if validator(passport) {
			validPassports++
		}
	}
	return validPassports
}

func part01(rows []string) int {

	validPassports := validatePasswords(rows, isValidPassportPart01())
	fmt.Println("Part01", validPassports)
	return validPassports
}

func part02(rows []string) int {
	validPassports := validatePasswords(rows, isValidPassportPart02())
	fmt.Println("Part02", validPassports)
	return validPassports
}

func main() {

	rows := rowsAsStringArray("input")

	part01(rows)

	part02(rows)
}
