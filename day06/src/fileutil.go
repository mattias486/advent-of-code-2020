package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func rowsAsStringArray(path string) []string {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	strings := strings.Split(string(bytes), "\n")
	// skip last empty item after last new line
	return strings[0 : len(strings)-1]
}
