package main

import "fmt"

func part01(rows []string) int {
	rowsGrouped := rowsGrouped(rows)
	return countSumOfAllGroups(rowsGrouped, countUniquePerGroup())
}

func part02(rows []string) int {
	rowsGrouped := rowsGrouped(rows)
	return countSumOfAllGroups(rowsGrouped, countQuestionsAllGroupMembersAnswered())
}

type GroupSumCounter func(answers map[rune]int, groupSize int) int

func countSumOfAllGroups(rowsGrouped [][]string, counter GroupSumCounter) int {
	sum := 0
	for _, rowsForGroup := range rowsGrouped {
		groupAnswerMap := getGroupAnswerMap(rowsForGroup)
		sum += counter(groupAnswerMap, len(rowsForGroup))
	}
	return sum
}

func getGroupAnswerMap(groupRows []string) map[rune]int {
	answers := make(map[rune]int)
	for _, row := range groupRows {
		for _, char := range row {
			if val, ok := answers[char]; ok {
				answers[char] = val + 1
				continue
			}
			answers[char] = 1
		}
	}
	return answers
}

func countUniquePerGroup() GroupSumCounter {
	return func(answers map[rune]int, groupSize int) int {
		return len(answers)
	}
}

func countQuestionsAllGroupMembersAnswered() GroupSumCounter {
	return func(answers map[rune]int, groupSize int) int {
		allAnswered := 0
		for key, _ := range answers {
			if answers[key] == groupSize {
				allAnswered++
			}
		}
		return allAnswered
	}
}

func rowsGrouped(rows []string) [][]string {
	group := []string{}
	allGroups := [][]string{}
	for _, val := range rows {
		if val == "" {
			allGroups = append(allGroups, group)
			group = []string{}
			continue
		}
		group = append(group, val)
	}
	allGroups = append(allGroups, group)
	return allGroups
}

func main() {

	rows := rowsAsStringArray("input")

	part01 := part01(rows)

	fmt.Println("Part 01", part01)

	part02 := part02(rows)

	fmt.Println("Part 02", part02)
}
