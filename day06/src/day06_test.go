package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`abc

a
b
c

ab
ac

a
a
a
a

b`, "\n")

	expected := 11
	got := part01(input)
	if got != expected {
		t.Errorf("Part01 = %d; want %d", got, expected)
	}
}

func TestPart02(t *testing.T) {

	input := strings.Split(`abc

a
b
c

ab
ac

a
a
a
a

b`, "\n")

	expected := 6
	got := part02(input)
	if got != expected {
		t.Errorf("Part02 = %d; want %d", got, expected)
	}
}
