package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type Password struct {
	min       int
	max       int
	character string
	value     string
}

type PasswordValidator func(password Password) bool

func rowsAsStringArray(path string) []string {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	strings := strings.Split(string(bytes), "\n")
	// skip last empty item after last new line
	return strings[0 : len(strings)-1]
}

func passwordFromRows(rows []string) []Password {
	var result []Password
	for _, row := range rows {
		rowSections := strings.Split(row, " ")
		minAndMax := strings.Split(rowSections[0], "-")
		character := string(rowSections[1])[0:1]
		password := string(rowSections[2])
		min, _ := strconv.Atoi(minAndMax[0])
		max, _ := strconv.Atoi(minAndMax[1])

		pass_struct := Password{min, max, character, password}

		result = append(result, pass_struct)
	}
	return result
}

func isPasswordValidPart01() PasswordValidator {
	return func(password Password) bool {
		numberOfMatches := 0
		for _, char := range password.value {
			if string(char) == password.character {
				numberOfMatches++
			}
		}
		return numberOfMatches >= password.min && numberOfMatches <= password.max
	}
}

func passwordMatchesOnPosition(character string, passwordValue string, index int) bool {
	if len(passwordValue) <= index {
		return false
	}
	return passwordValue[index:index+1] == character
}

func isPasswordValidPart02() PasswordValidator {
	return func(password Password) bool {
		//-1 to translate position to index
		firstIndexMatch := passwordMatchesOnPosition(password.character, password.value, password.min-1)
		secondIndexMatch := passwordMatchesOnPosition(password.character, password.value, password.max-1)

		if firstIndexMatch && secondIndexMatch {
			return false
		}
		return firstIndexMatch || secondIndexMatch
	}
}

func getNumberOfValidMatches(passwords []Password, validator PasswordValidator) int {
	numberOfMatches := 0
	for _, password := range passwords {
		if validator(password) {
			numberOfMatches++
		}
	}
	return numberOfMatches
}

func main() {
	path := "input"
	rows := rowsAsStringArray(path)
	passwords := passwordFromRows(rows)

	numberOfValidPasswordsPart01 := getNumberOfValidMatches(passwords, isPasswordValidPart01())
	fmt.Println("Part01 :", numberOfValidPasswordsPart01)

	numberOfValidPasswordsPart02 := getNumberOfValidMatches(passwords, isPasswordValidPart02())
	fmt.Println("Part02 :", numberOfValidPasswordsPart02)

}
