package main

import (
	"fmt"
	"strconv"
	"strings"
)

func part01(rows []string) int {

	instructions := rowsToInstructions(rows)

	result, _ := runInstructionsUntilDuplicate(instructions)

	return result
}

type Instruction struct {
	name  string
	value int
}

func rowsToInstructions(rows []string) []Instruction {
	result := []Instruction{}
	for _, rowItem := range rows {
		rowParts := strings.Split(rowItem, " ")
		rowValue, _ := strconv.Atoi(rowParts[1])
		result = append(result, Instruction{rowParts[0], rowValue})
	}
	return result
}

// returns : acc, ifDuplicatesWasEncountered
func runInstructionsUntilDuplicate(instructions []Instruction) (int, bool) {

	instructionLog := map[int]int{}
	acc := 0

	currentInstructionIndex := 0
	for {
		if _, ok := instructionLog[currentInstructionIndex]; ok {
			return acc, true
		}
		if currentInstructionIndex == len(instructions) {
			return acc, false
		}
		currentInstruction := instructions[currentInstructionIndex]
		instructionLog[currentInstructionIndex] = 1

		switch currentInstruction.name {
		case "nop":
			currentInstructionIndex++
		case "jmp":
			currentInstructionIndex += currentInstruction.value
		case "acc":
			acc += currentInstruction.value
			currentInstructionIndex++
		}
	}
}

func getInstructionsWithModifiedInstruction(instructions []Instruction, indexToSwitch int) []Instruction {
	result := make([]Instruction, len(instructions))
	copy(result, instructions)
	switch result[indexToSwitch].name {
	case "jmp":
		result[indexToSwitch].name = "nop"
	case "nop":
		result[indexToSwitch].name = "jmp"
	default:
		fmt.Println("Invalid instruction switching", indexToSwitch)
	}
	return result
}

func part02(rows []string) int {

	instructions := rowsToInstructions(rows)

	for i, instructionItem := range instructions {
		if instructionItem.name == "acc" {
			continue
		}
		modifiedInstructions := getInstructionsWithModifiedInstruction(instructions, i)
		result, duplicates := runInstructionsUntilDuplicate(modifiedInstructions)

		if duplicates == false {
			return result
		}
	}
	return -1
}

func main() {

	rows := rowsAsStringArray("input")

	accPart01 := part01(rows)

	fmt.Println("Part 01", accPart01)

	accPart02 := part02(rows)

	fmt.Println("Part 02", accPart02)
}
