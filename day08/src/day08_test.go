package main

import (
	"strings"
	"testing"
)

func TestPart01(t *testing.T) {

	input := strings.Split(`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, "\n")

	got := part01(input)
	expected := 5

	if got != expected {
		t.Errorf("Part 01 = %d; want : %d,", got, expected)
	}

}

func TestPart02(t *testing.T) {

	input := strings.Split(`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, "\n")

	got := part02(input)
	expected := 8

	if got != expected {
		t.Errorf("Part 02 = %d; want : %d,", got, expected)
	}

}
